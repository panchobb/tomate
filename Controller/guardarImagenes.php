<?php
require_once 'basededatos.php';
require_once 'helpers.php';

function ifEmpty($archi, $fallback, $name){
  $img1;
  if ($archi['name'] !== '') {
    return $img1 = guardarFoto($archi, $name);
  }
  return $img1 = $fallback; 
}

if($_POST){
  //dd($_POST,$_FILES);
  $hero = [
    'titulo' => 'hero',
    'foto' => ifEmpty($_FILES['fotohero'],$_POST['fallbackIMGhero'],'hero-img')
  ];
  $comparti = [
    'titulo' => 'comparti',
    'foto' => ifEmpty($_FILES['fotocomparti'],$_POST['fallbackIMGcomparti'], 'comparti-img')
  ];
  $nosotros = [
    'titulo' => 'nosotros',
    'foto' => ifEmpty($_FILES['fotonosotros'],$_POST['fallbackIMGnosotros'], 'nosotros-img')
  ];

  //dd("Hola Fotos",$hero, $comparti, $nosotros);

  guardarIMG($hero,true);
  guardarIMG($comparti);
  guardarIMG($nosotros);

  //dd(traerIMGs());
  redirect('../index.php');
}
