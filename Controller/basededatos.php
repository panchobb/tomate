<?php
function guardarFoto($foto,$name)
{
  /* if ($foto['name'] !== '') { */
    $nombre = $foto["name"];
    $archivo = $foto["tmp_name"];
    $ext = pathinfo($nombre, PATHINFO_EXTENSION);
    $nombreFinal = $name . "." . $ext;
    $miArchivo = realpath(dirname(__FILE__) . '/..');
    $miArchivo = $miArchivo . "/img/";
    $miArchivo = $miArchivo . $nombreFinal;
    move_uploaded_file($archivo, $miArchivo);
  /* } */
  return $nombreFinal;
}

function guardar($elemento,$borrar = false)
{
  $eleJson = json_encode($elemento);
  if($borrar === true){
    file_put_contents(realpath(dirname(__FILE__) . '/..').'/tomate.json', $eleJson . PHP_EOL);
  } else {
    file_put_contents(realpath(dirname(__FILE__) . '/..') . '/tomate.json', $eleJson . PHP_EOL, FILE_APPEND);
  }
}

function traer()
{
  $arrayCosas = [];
  $archivo = fopen(realpath(dirname(__FILE__) . '/..').'/tomate.json', 'r');
  while (($linea = fgets($archivo)) !== false) {
    if($linea !== ''){
      $arrayCosas[] = json_decode($linea, true);
    }
  }
  fclose($archivo);

  return $arrayCosas;
}

function traerUsuario($titulo)
{
  $nota = null;
  $archivo = fopen(realpath(dirname(__FILE__) . '/..').'/tomate.json', 'r');
  while (($linea = fgets($archivo)) !== false) {
    $notaActual = json_decode($linea, true);
    if ($notaActual['titulo'] === $titulo) {
      $nota = $notaActual;
      break;
    }
  }
  fclose($archivo);

  return $nota;
}

/* GUARDAR PDF */
function crearPDF($pdf, $name)
{
  $nombreFinal = $name . ".pdf";
  if($pdf['name'] !== ''){
    $nombre = $pdf["name"];
    $archivo = $pdf["tmp_name"];
    $ext = pathinfo($nombre, PATHINFO_EXTENSION);
    $nombreFinal = $name . "." . $ext;
    $miArchivo = realpath(dirname(__FILE__) . '/..');
    $miArchivo = $miArchivo . "/pdf/";
    $miArchivo = $miArchivo . $nombreFinal;
    move_uploaded_file($archivo, $miArchivo);
  }

  return $nombreFinal;
}

function guardarPDF($elemento, $borrar = false)
{
  $eleJson = json_encode($elemento);
  if ($borrar === true) {
    file_put_contents(realpath(dirname(__FILE__) . '/..') . '/pdfs.json', $eleJson . PHP_EOL);
  } else {
    file_put_contents(realpath(dirname(__FILE__) . '/..') . '/pdfs.json', $eleJson . PHP_EOL, FILE_APPEND);
  }
}

function traerPDFs()
{
  $arrayCosas = [];
  $archivo = fopen(realpath(dirname(__FILE__) . '/..') . '/pdfs.json', 'r');
  while (($linea = fgets($archivo)) !== false) {
    if ($linea !== '') {
      $arrayCosas[] = json_decode($linea, true);
    }
  }
  fclose($archivo);

  return $arrayCosas;
}
/* GUARDAR IMGs */

function guardarIMG($ruta, $borrar = false)
{
  $eleJson = json_encode($ruta);
  if ($borrar === true) {
    file_put_contents(realpath(dirname(__FILE__) . '/..') . '/imagenes.json', $eleJson . PHP_EOL);
  } else {
    file_put_contents(realpath(dirname(__FILE__) . '/..') . '/imagenes.json', $eleJson . PHP_EOL, FILE_APPEND);
  }
}

function traerIMGs()
{
  $arrayCosas = [];
  $archivo = fopen(realpath(dirname(__FILE__) . '/..') . '/imagenes.json', 'r');
  while (($linea = fgets($archivo)) !== false) {
    if ($linea !== '') {
      $arrayCosas[] = json_decode($linea, true);
    }
  }
  fclose($archivo);
  $array = [
    'hero' => $arrayCosas[0]['foto'],
    'comparti' => $arrayCosas[1]['foto'],
    'nosotros' => $arrayCosas[2]['foto']
  ];

  return $array;
}