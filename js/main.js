const menu = document.querySelector('svg[data-icon="bars"]');
const close = document.querySelector('svg[data-icon="times"]');
const nav = document.querySelector('.nav')

//const icon = document.querySelector('svg[data-icon="bars"]');
const logo = document.querySelector('svg[data-name="tomateLogo"]');

menu.addEventListener("click",toggleMenu);
close.addEventListener('click',toggleMenu);
//close.addEventListener("click",toggleMenu());

function toggleMenu() {
  menuVissibility();
}
function menuVissibility(){
  var width = (window.innerWidth > 0) ? window.innerWidth : screen.width;
  console.log(width);
  
  var isMobile = false;
  if (width < 768) {
    isMobile = true;
  }
  console.log(isMobile);
  
  close.style.zIndex = "999";
  menu.style.zIndex = "999";

  if (nav.style.opacity == 1){
    nav.style.opacity = "0";
    nav.style.zIndex = "-1";
    close.classList.add('hidden');
    menu.classList.remove("hidden");
    close.style.filter = 'brightness(1)';
    logo.style.filter = 'brightness(1)';
  } else if (nav.style.opacity == 0 && isMobile == true){
    nav.style.opacity = "1";
    nav.style.zIndex = "899";
    menu.classList.add('hidden');
    close.classList.remove("hidden");
    close.style.filter = 'brightness(100)';
    logo.style.filter = 'brightness(100)';
  } else if (nav.style.opacity == 0 && isMobile == false) {
    nav.style.opacity = "1";
    nav.style.zIndex = "859";
    menu.classList.add('hidden');
    close.classList.remove("hidden");
    close.style.filter = 'brightness(100)';
    logo.style.filter = 'brightness(100)';
  }
}

