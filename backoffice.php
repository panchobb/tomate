<?php 
require_once 'funciones.php';

$notas = traer();
$pdfs = traerPDFs();
$fotos = traerIMGs();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title> Tomate | Backoffice </title> <!-- Logica de Title por pagina -->
  <link rel="stylesheet" href="css/reset.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.1/css/bulma.min.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
  <link rel="stylesheet" href="css/backoffice.css">
</head>
<body>
  <nav class="navbar" role="navigation" aria-label="dropdown navigation">
    <a class="navbar-item" href="/">
      <img src="img/LOGO-01.png" alt="logo de Tomate" height="60">
    </a>
  </nav>
  <main>
    <section class="backoffice">
      <header>
        <p class="title is-2 is-spaced" >backoffice</p>
      </header>
      <?php $ite = 0 ?>
      <article class="is-three-fifths is-offset-one-fifth">
        <p class="title is-3 is-spaced">Fotos</p>
      </article>
      <form class="columns" method="post" action="Controller/guardarImagenes.php" enctype='multipart/form-data'>
        <?php foreach ($fotos as $titulo => $foto) : ?>
        <fieldset class="column">
          <figure class="image">
            <img src="img/<?= $foto ?>" alt="<?= $titulo ?>">
          </figure>
          <input type="text" name="fallbackIMG<?= $titulo ?>" value="<?= $foto ?>" hidden>
          <div class="file has-name is-fullwidth">
            <label class="file-label">
              <input class="file-input" type="file" name="foto<?= $titulo ?>" id="foto<?= $titulo ?>">
              <span class="file-cta">
                <span class="file-icon">
                  <i class="fas fa-upload"></i>
                </span>
                <span class="file-label">
                  Subi una Imagen
                </span>
              </span>
              <span class="file-name" id="foto<?= $titulo ?>name">
                img/<?= $foto ?>
              </span>
            </label>
          </div>
          <button type="submit" class="button is-success is-rounded"><i class="far fa-save"></i>Guardar</button>
        </fieldset>
        <?php endforeach; ?>
      </form>

      <!--  -->
      <?php $ite = 0 ?>
      <article class="is-three-fifths is-offset-one-fifth">
        <p class="title is-3 is-spaced">Notas y Eventos</p>
      </article>
      <form class="columns" method="post" action="Controller/guardar.php" enctype='multipart/form-data'>
        <?php foreach ($notas as $nota): ?>
        <?php $ite++ ?>
        <fieldset class="column">
          <figure class="image is-square">
            <img class="max-256" src="img/<?= $nota['foto'] ?>" alt="<?= $nota['titulo'] ?>">
          </figure>
          <input type="text" name="fallbackIMG<?= $ite ?>" value="<?= $nota['foto'] ?>" hidden>
          <div class="file has-name is-fullwidth">
            <label class="file-label">
              <input class="file-input" type="file" name="foto<?= $ite ?>" id="foto<?= $ite ?>">
              <span class="file-cta">
                <span class="file-icon">
                  <i class="fas fa-upload"></i>
                </span>
                <span class="file-label">
                  Subi una Imagen
                </span>
              </span>
              <span class="file-name" id="foto<?= $ite ?>name">
                img/<?= $nota['foto'] ?>
              </span>
            </label>
          </div>
          <div class="control">
            <textarea class="textarea is-medium" type="text" name="titulo<?= $ite ?>"><?= $nota['titulo'] ?>
            </textarea>
          </div>
          <div class="control">
            <textarea class="textarea is-small" type="text" name="texto<?= $ite ?>"><?= $nota['texto'] ?>
            </textarea>
          </div>
          <input type="text" class="input is-rounded" name="fecha<?= $ite ?>" value="<?= $nota['fecha'] ?>" >
          <input type="text" class="input is-rounded" name="link<?= $ite ?>" value="<?= $nota['link'] ?>" >
          <button type="submit" class="button is-success is-rounded"><i class="far fa-save"></i>Guardar</button>
        </fieldset>
        <?php endforeach; ?>
      </form>
      <!--  -->
      <article class="is-three-fifths is-offset-one-fifth">
        <p class="title is-3 is-spaced">Subir PDFs</p>
      </article>
      <form class="" method="post" action="Controller/guardarpdf.php" enctype='multipart/form-data'>
        <?php $ite = 0; ?>
        <?php foreach ($pdfs as $pdf) : ?>
        <?php $ite++; ?>
        <fieldset class="is-4">
          <p class="subtitle is-5"><?= $pdf['titulo'] ?></p>
          <input type="text" hidden name="title<?= $ite ?>" value="<?= $pdf['titulo'] ?>">
          <div class="file has-name is-fullwidth">
            <label class="file-label">
              <input class="file-input" type="file" name="pdf<?= $ite ?>" id="pdf<?= $ite ?>">
              <span class="file-cta">
                <span class="file-icon">
                  <i class="fas fa-upload"></i>
                </span>
                <span class="file-label">
                  Subi un PDF
                </span>
              </span>
              <span class="file-name" id="pdf<?= $ite ?>name">
                pdf/<?= $pdf['pdf'] ?>
              </span>
            </label>
          </div>
          <button type="submit" class="button is-success is-rounded"><i class="far fa-save"></i>Guardar</button>
        </fieldset>
        <?php endforeach; ?>
      </form>
    </section>
  </main>
  <script>

    var foto1 = document.getElementById("foto1");
    var foto2 = document.getElementById("foto2");
    var foto3 = document.getElementById("foto3");
    var pdf1  = document.getElementById("pdf1");
    var pdf2  = document.getElementById("pdf2");
    var hero = document.getElementById('fotohero');
    var comparti = document.getElementById('fotocomparti');
    
    var foto1name = document.getElementById("foto1name");
    var foto2name = document.getElementById("foto2name");
    var foto3name = document.getElementById("foto3name");
    var pdf1name  = document.getElementById("pdf1name");
    var pdf2name  = document.getElementById("pdf2name");
    var heroname = document.getElementById('fotoheroname');
    var compartiname = document.getElementById('fotocompartiname');

    var nosotros = document.getElementById('fotonosotros');
    var nosotrosname = document.getElementById('fotonosotrosname');

    foto1.onchange = function(){
      if(foto1.files.length > 0)
      {
        foto1name.innerHTML = foto1.files[0].name;
      }
    };
    foto2.onchange = function(){
      if(foto2.files.length > 0)
      {
        foto2name.innerHTML = foto2.files[0].name;
      }
    };
    foto3.onchange = function(){
      if(foto3.files.length > 0)
      {
        foto3name.innerHTML = foto3.files[0].name;
      }
    };
    pdf1.onchange = function(){
      if(pdf1.files.length > 0)
      {
        pdf1name.innerHTML = pdf1.files[0].name;
      }
    };
    pdf2.onchange = function(){
      if(pdf2.files.length > 0)
      {
        pdf2name.innerHTML = pdf2.files[0].name;
      }
    };
    fotohero.onchange = function(){
      if(fotohero.files.length > 0)
      {
        fotoheroname.innerHTML = fotohero.files[0].name;
      }
    };
    fotocomparti.onchange = function(){
      if(fotocomparti.files.length > 0)
      {
        fotocompartiname.innerHTML = fotocomparti.files[0].name;
      }
    };
    fotonosotros.onchange = function(){
      if(fotonosotros.files.length > 0)
      {
        fotonosotrosname.innerHTML = fotonosotros.files[0].name;
      }
    };
  </script>
</body>
</html>