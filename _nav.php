<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NLTDTM3"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<nav class="nav-bar">
  <article class="nav-bar_icons">
    <i class="clickable nav-bar_icon">
      <svg aria-hidden="true" data-icon="bars" class="icon icon_menu" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
        <path class="svg_red" d="M436 124H12c-6.627 0-12-5.373-12-12V80c0-6.627 5.373-12 12-12h424c6.627 0 12 5.373 12 12v32c0 6.627-5.373 12-12 12zm0 160H12c-6.627 0-12-5.373-12-12v-32c0-6.627 5.373-12 12-12h424c6.627 0 12 5.373 12 12v32c0 6.627-5.373 12-12 12zm0 160H12c-6.627 0-12-5.373-12-12v-32c0-6.627 5.373-12 12-12h424c6.627 0 12 5.373 12 12v32c0 6.627-5.373 12-12 12z"></path>
      </svg>
    </i>
    <i class="clickable nav-bar_icon">
      <svg aria-hidden="true" data-icon="times" class="icon icon_close hidden" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512">
        <path class="svg_red" d="M207.6 256l107.72-107.72c6.23-6.23 6.23-16.34 0-22.58l-25.03-25.03c-6.23-6.23-16.34-6.23-22.58 0L160 208.4 52.28 100.68c-6.23-6.23-16.34-6.23-22.58 0L4.68 125.7c-6.23 6.23-6.23 16.34 0 22.58L112.4 256 4.68 363.72c-6.23 6.23-6.23 16.34 0 22.58l25.03 25.03c6.23 6.23 16.34 6.23 22.58 0L160 303.6l107.72 107.72c6.23 6.23 16.34 6.23 22.58 0l25.03-25.03c6.23-6.23 6.23-16.34 0-22.58L207.6 256z"></path>
      </svg>
    </i>
  </article>
  <article class="nav-bar_logo">
    <i><a href="index.php">
      <svg class="desktop-hidden" id="logoB" data-name="tomateLogo" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 197.52 38.55">
        <g id="_Grupo_" data-name="Grupo">
          <path id="hojaDeTomate" data-name="Trazado" class="svg_green" d="M36.54,6C34.73,4.14,35,2.11,36.79.8c1.59-1.14,3.54-1.4,4.26,1.58-.7,2.53-2.82,2.71-4.51,3.61"/>
          <path id="oDeTomate" data-name="Trazado compuesto" class="svg_red" d="M50.21,19h0a14.15,14.15,0,0,0-7.52-7.51,14.24,14.24,0,0,0-10.95,0A14.12,14.12,0,0,0,24.22,19a14.25,14.25,0,0,0,0,10.93,14.07,14.07,0,0,0,7.53,7.53,14.15,14.15,0,0,0,10.94,0,13.92,13.92,0,0,0,4.5-3,14.15,14.15,0,0,0,3-15.45Zm-4.65,8.93h0a8.63,8.63,0,0,1-2,2.92,8.54,8.54,0,0,1-2.9,2,9.24,9.24,0,0,1-6.91,0,8.73,8.73,0,0,1-2.92-2,8.56,8.56,0,0,1-2-2.9,9.24,9.24,0,0,1,0-6.91,8.68,8.68,0,0,1,2-2.92,8.62,8.62,0,0,1,2.89-2,9.24,9.24,0,0,1,6.91,0,8.49,8.49,0,0,1,2.92,2,8.5,8.5,0,0,1,2,2.89,9.24,9.24,0,0,1,0,6.91Z"/>
          <rect id="segundoSlash" data-name="Trazado" class="svg_red" x="140.29" y="20.22" width="7.16" height="5.2"/>
          <rect id="primerSlash" data-name="Trazado" class="svg_red" x="58.24" y="20.22" width="7.16" height="5.2"/>
          <polygon id="eDeTomate" data-name="Trazado" class="svg_red" points="177.2 37.84 177.2 7 197.52 7 197.52 12.63 183.01 12.63 183.01 21.3 195.66 21.26 195.66 27.08 183.01 27.06 183.01 32.12 197.13 32.12 197.13 37.84 177.2 37.84"/>
          <path id="tDeTomate1" data-name="Trazado compuesto" class="svg_red" d="M7.58,37.84V12.67H0V6.94H21v5.73H13.39V37.84Z"/>
          <path id="mDeTomate" data-name="Trazado compuesto" class="svg_red" d="M95.87,37.84V17.22L88,28.22l-7.8-11V37.84H74.62V7h5.27L88,18.48,96.17,7h5.31V37.84Z"/>
          <path id="aDeTomate" data-name="Trazado compuesto" class="svg_red" d="M129.71,37.84l-3.08-8.32h-9.95l-3.12,8.32h-6L119.08,7h5.1l11.6,30.84Zm-5.27-14-2.79-7.5-2.78,7.5Z"/>
          <path id="tDeTomate" data-name="Trazado compuesto" class="svg_red" d="M157.32,37.84V12.67h-7.58V6.94h21v5.73h-7.58V37.84Z"/>
        </g>
      </svg>
          <svg class="desktop-hidden hidden" id="logoC" data-name="tomateLogo" xmlns="http://www.w3.org/2000/svg" viewBox="20 0 40.52 38.55">
        <g id="_Grupo_" data-name="Grupo">
          <path id="hojaDeTomate" data-name="Trazado" class="svg_green" d="M36.54,6C34.73,4.14,35,2.11,36.79.8c1.59-1.14,3.54-1.4,4.26,1.58-.7,2.53-2.82,2.71-4.51,3.61"/>
          <path id="oDeTomate" data-name="Trazado compuesto" class="svg_red" d="M50.21,19h0a14.15,14.15,0,0,0-7.52-7.51,14.24,14.24,0,0,0-10.95,0A14.12,14.12,0,0,0,24.22,19a14.25,14.25,0,0,0,0,10.93,14.07,14.07,0,0,0,7.53,7.53,14.15,14.15,0,0,0,10.94,0,13.92,13.92,0,0,0,4.5-3,14.15,14.15,0,0,0,3-15.45Zm-4.65,8.93h0a8.63,8.63,0,0,1-2,2.92,8.54,8.54,0,0,1-2.9,2,9.24,9.24,0,0,1-6.91,0,8.73,8.73,0,0,1-2.92-2,8.56,8.56,0,0,1-2-2.9,9.24,9.24,0,0,1,0-6.91,8.68,8.68,0,0,1,2-2.92,8.62,8.62,0,0,1,2.89-2,9.24,9.24,0,0,1,6.91,0,8.49,8.49,0,0,1,2.92,2,8.5,8.5,0,0,1,2,2.89,9.24,9.24,0,0,1,0,6.91Z"/>

        </g>
      </svg>
    </a></i>
  </article>
</nav>

<section class="nav" style="opacity:0;">
  <nav class="nav-links">
    <a href="#disfruta">sabores</a>
    <a href="#aprende">workshops</a>
    <a href="#comparti">eventos</a>
    <a href="#nosotros">nosotros</a>
    <a href="#contacto">contacto</a>
  </nav>
  <nav class="nav-social">
    <a href="http://" target="_blank" rel="noopener noreferrer">
      <svg aria-hidden="true" data-prefix="fab" data-icon="facebook-f" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 264 512">
        <path class="social_icon facebook" d="M76.7 512V283H0v-91h76.7v-71.7C76.7 42.4 124.3 0 193.8 0c33.3 0 61.9 2.5 70.2 3.6V85h-48.2c-37.8 0-45.1 18-45.1 44.3V192H256l-11.7 91h-73.6v229"></path>
      </svg>
    </a>
    <a href="http://" target="_blank" rel="noopener noreferrer">
      <svg aria-hidden="true" data-prefix="fab" data-icon="instagram" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
        <path class="social_icon instagram" d="M224.1 141c-63.6 0-114.9 51.3-114.9 114.9s51.3 114.9 114.9 114.9S339 319.5 339 255.9 287.7 141 224.1 141zm0 189.6c-41.1 0-74.7-33.5-74.7-74.7s33.5-74.7 74.7-74.7 74.7 33.5 74.7 74.7-33.6 74.7-74.7 74.7zm146.4-194.3c0 14.9-12 26.8-26.8 26.8-14.9 0-26.8-12-26.8-26.8s12-26.8 26.8-26.8 26.8 12 26.8 26.8zm76.1 27.2c-1.7-35.9-9.9-67.7-36.2-93.9-26.2-26.2-58-34.4-93.9-36.2-37-2.1-147.9-2.1-184.9 0-35.8 1.7-67.6 9.9-93.9 36.1s-34.4 58-36.2 93.9c-2.1 37-2.1 147.9 0 184.9 1.7 35.9 9.9 67.7 36.2 93.9s58 34.4 93.9 36.2c37 2.1 147.9 2.1 184.9 0 35.9-1.7 67.7-9.9 93.9-36.2 26.2-26.2 34.4-58 36.2-93.9 2.1-37 2.1-147.8 0-184.8zM398.8 388c-7.8 19.6-22.9 34.7-42.6 42.6-29.5 11.7-99.5 9-132.1 9s-102.7 2.6-132.1-9c-19.6-7.8-34.7-22.9-42.6-42.6-11.7-29.5-9-99.5-9-132.1s-2.6-102.7 9-132.1c7.8-19.6 22.9-34.7 42.6-42.6 29.5-11.7 99.5-9 132.1-9s102.7-2.6 132.1 9c19.6 7.8 34.7 22.9 42.6 42.6 11.7 29.5 9 99.5 9 132.1s2.7 102.7-9 132.1z"></path>
      </svg>
    </a>
  </nav>
</section>